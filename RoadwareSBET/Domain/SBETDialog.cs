﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoadwareSBETClassLibrary.Domain;

namespace RoadwareSBET.Domain
{
  public partial class frmSBETDialog : Form
  {
    #region fields
    private DataSet fNfDs;

    private DataTable fNfDt;
    private DataTable projDt;
    private DataTable batchDt = new DataTable();

    private string fNfSBETPOSPacExportPy;
    private string posPacExe;
    private string prjcts = string.Empty;
    private string pyExeFlNm;
    private string strLocalProjectFolder;
    private string strServerProjectFolder;
    private string strTemplateFile;

    private string xmlFilesAndFoldersNm = "FoldersAndFiles.xml";
    private string xmlGNSSModesNm = @"Data\GNSSModes.xml";

    private TimeSpan timeLeft;
    #endregion

    #region constructors
    public frmSBETDialog()
    {
      InitializeComponent();
      InitializeControls();
    }
    #endregion

    #region methods
    private void BackupToServer(string prjNmBatNm)
    {

      string btchPrjFullFlNm = Path.Combine(strLocalProjectFolder,
                                            string.Format("{0}.posbat", prjNmBatNm));
      if (File.Exists(btchPrjFullFlNm))
      {
        string xmlPrjFullFlNm = string.Empty;
        if (prjNmBatNm.Substring(prjNmBatNm.Length - 3, 3).ToUpper() != "BAD")
        {
          xmlPrjFullFlNm = Path.Combine(strLocalProjectFolder,
                                        string.Format("{0}.xml", prjNmBatNm));
        }
        string srcFldr = Path.Combine(strLocalProjectFolder, prjNmBatNm);
        Common cmn = new Common();
        string svrFldr = cmn.GetServerFolderFromPosBatFile(btchPrjFullFlNm);
        if (!string.IsNullOrEmpty(svrFldr) && Directory.Exists(srcFldr))
        {
          //Copy the .posbat folder results
          cmn.DirectoryCopy(srcFldr, Path.Combine(svrFldr, prjNmBatNm), true, true);

          //Copy the .posbat file
          File.Copy(btchPrjFullFlNm, Path.Combine(svrFldr, string.Format("{0}.posbat", prjNmBatNm)), true);
          File.Delete(btchPrjFullFlNm);

          //Copy the .xml file
          if (!string.IsNullOrEmpty(xmlPrjFullFlNm))
          {
            File.Copy(xmlPrjFullFlNm, Path.Combine(svrFldr, string.Format("{0}.xml", prjNmBatNm)), true);
            File.Delete(xmlPrjFullFlNm);
          }
        }
        else
        {
          MessageBox.Show(string.Format("Server folder, {0}, could not be found.", svrFldr),
                          "ERROR!!",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Error);
        }
      }
    }

    private void btnBackupToServer_Click(object sender, EventArgs e)
    {

      if (!IsBatchFileSelected()) { return; }
      Cursor.Current = Cursors.WaitCursor;
      ChangeControlStatus(false);
      btnProcessBadBatch.Enabled = false;
      DataRow[] drs = batchDt.Select("[Process] = True", "[Project Name], [Batch Name]");
      foreach (DataRow dr in drs)
      {


        string dbNmBatNm = string.Format("{0}_{1}", dr["Database Name"].ToString(),
                                                     dr["Batch Name"].ToString());

        lblStatus.Text = string.Format("Backing \"{0}\" up to server.", dbNmBatNm);
        lblStatus.Update();
        Application.DoEvents();

        BackupToServer(dbNmBatNm);

        lblStatus.Text = string.Format("\"{0}\" backed up to server.", dbNmBatNm);
        lblStatus.Update();
        Application.DoEvents();

        dbNmBatNm = string.Format("{0}_{1}_BAD", dr["Database Name"].ToString(),
                                                  dr["Batch Name"].ToString());
        lblStatus.Text = string.Format("Backing \"{0}\" up to server.", dbNmBatNm);
        lblStatus.Update();
        Application.DoEvents();

        BackupToServer(dbNmBatNm);

        lblStatus.Text = string.Format("\"{0}\" backed up to server.", dbNmBatNm);
        lblStatus.Update();
        Application.DoEvents();

      }

      lblStatus.Text = "All data backed up to server.";
      lblStatus.Update();

      Cursor.Current = Cursors.Default;
      ChangeControlStatus(true);
      btnProcessBadBatch.Enabled = true;
    }

    private void btnGetLocalFolder_Click(object sender, EventArgs e)//3
    {
      lblStatus.Text = "Getting local folder.";
      lblStatus.Update();
      FolderBrowserDialog fbDlg = new FolderBrowserDialog();
      if (!string.IsNullOrEmpty(txtBxLocalFolder.Text)) { fbDlg.SelectedPath = txtBxLocalFolder.Text; }
      if (fbDlg.ShowDialog() == DialogResult.OK)
      {
        txtBxLocalFolder.Text = fbDlg.SelectedPath;
        UpdateTextBoxChanges(ref txtBxLocalFolder, ref strLocalProjectFolder, "LocalProjectFolder");
        lblStatus.Text = "Local folder updated.";
        lblStatus.Update();
      }
      else
      {
        lblStatus.Text = "Local folder not updated.";
        lblStatus.Update();
      }
    }

    private void btnGetServerFolder_Click(object sender, EventArgs e)
    {
      lblStatus.Text = "Getting server folder.";
      lblStatus.Update();
      FolderBrowserDialog fbDlg = new FolderBrowserDialog();
      if (!string.IsNullOrEmpty(txtBxServerFolder.Text)) { fbDlg.SelectedPath = txtBxServerFolder.Text; }
      if (fbDlg.ShowDialog() == DialogResult.OK)
      {
        txtBxServerFolder.Text = fbDlg.SelectedPath;
        UpdateTextBoxChanges(ref txtBxServerFolder, ref strServerProjectFolder, "ServerProjectFolder");
        lblStatus.Text = "Server folder updated.";
        lblStatus.Update();
      }
      else
      {
        lblStatus.Text = "Server folder not updated.";
        lblStatus.Update();
      }

    }

    private void btnGetTemplateFile_Click(object sender, EventArgs e)//4
    {
      lblStatus.Text = "Getting template file.";
      lblStatus.Update();
      OpenFileDialog ofDlg = new OpenFileDialog();
      if (!string.IsNullOrEmpty(txtBxTemplateFile.Text))
      {
        ofDlg.InitialDirectory = Path.GetDirectoryName(txtBxTemplateFile.Text);
      }
      ofDlg.Filter = "postml Files (*.postml)|*.postml";
      ofDlg.FilterIndex = 1;
      ofDlg.Multiselect = false;
      ofDlg.Title = "Select POSPac Template File";
      if (ofDlg.ShowDialog() == DialogResult.OK)
      {
        txtBxTemplateFile.Text = ofDlg.FileNames[0];
        UpdateTextBoxChanges(ref txtBxTemplateFile, ref strTemplateFile, "TemplateFile");
        lblStatus.Text = "Template file updated.";
        lblStatus.Update();
      }
      else
      {
        lblStatus.Text = "Template file not updated.";
        lblStatus.Update();
      }
    }

    private void btnRefreshProjects_Click(object sender, EventArgs e)
    {
      LoadProjects();
    }

    private void btnSBETExport_Click(object sender, EventArgs e)
    {
      lblStatus.Text = "Starting SBET export process.";
      lblStatus.Update();
      Form btchExpFrm = new frmSBETBatchExport(txtBxLocalFolder.Text,
                                               pyExeFlNm,
                                               fNfSBETPOSPacExportPy,
                                               posPacExe);
      btchExpFrm.ShowDialog();
      lblStatus.Text = "SBET export process completed.";
      lblStatus.Update();
    }

    private void btnStartBatchProcess_Click(object sender, EventArgs e)//5
    {
      Cursor.Current = Cursors.WaitCursor;
      //check if projects have been loaded.
      if (!IsProjectLoaded()) { return; }
      //check if any batch file have been selected.
      if (!IsBatchFileSelected()) { return; }

      if (btnStartBatchProcess.Text == "Cancel")
      {
        ChangeControlStatus(true);
        tmrStartDateAndTime.Stop();
        lblStatus.Text = "Batch process cancelled.";
        lblStatus.Update();
        btnStartBatchProcess.Text = "Start Batch Process";
        return;
      }
      else
      {
        ChangeControlStatus(false);
        btnStartBatchProcess.Text = "Cancel";
      }
      //check if any text box values have changed and update them
      UpdateTextBoxChanges(ref txtBxLocalFolder, ref strLocalProjectFolder, "LocalProjectFolder");
      UpdateTextBoxChanges(ref txtBxServerFolder, ref strServerProjectFolder, "ServerProjectFolder");
      UpdateTextBoxChanges(ref txtBxTemplateFile, ref strTemplateFile, "TemplateFile");

      lblStatus.Text = "Setting up batch(es) for processing.";
      lblStatus.Update();
      SetupBatchesForProcessing();
      lblStatus.Text = "Batch(es) set up for processing.";
      lblStatus.Update();

      timeLeft = dtTmPkrStartDateAndTime.Value.Subtract(DateTime.Now);
      tmrStartDateAndTime.Start();

        //where is it running everything
    }

    private void ChangeControlStatus(bool enableMode)
    {
      btnGetLocalFolder.Enabled = enableMode;
      btnRefreshProjects.Enabled = enableMode;
      btnGetServerFolder.Enabled = enableMode;
      btnGetTemplateFile.Enabled = enableMode;
      btnBackupToServer.Enabled = enableMode;
      //btnSBETExport.Enabled = enableMode;
      cmboBxGNSSMode.Enabled = enableMode;
      cmboBxProjects.Enabled = enableMode;
      dtTmPkrStartDateAndTime.Enabled = enableMode;
      grpBxProcessType.Enabled = enableMode;
      rdoBtnInternetExplorer.Enabled = enableMode;
      rdoBtnGoogleChrome.Enabled = enableMode;
    }

    private void cmboBxGNSSMode_SelectedIndexChanged(object sender, EventArgs e)
    {
      lblStatus.Text = string.Empty;
      lblStatus.Update();
    }

    private void cmboBxProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
      lblStatus.Text = string.Empty;
      lblStatus.Update();
      if (batchDt.Rows.Count > 0)
      {
        LoadBatches();
      }
    }

    private void CreateDataTableStructures()//1.1
    {
      List<DataTableField> flds = new List<DataTableField>();
      DataTableField dtFld;
      Common cmn = new Common();

      //Session Data Table
      dtFld = new DataTableField();
      dtFld.Name = "Project Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "GPS Post-Processing Imported Date"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "GPS Post-Processing Required Date"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "GPS Post-Processing Technician"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Batch Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "ARAN"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Data Collection From Date"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Data Collection To Date"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Server Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Database Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Video Location"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      projDt = cmn.CreateNewDataTable(flds, "Projects Data Table");
    }

    private void dtaGrdVwBatchDt_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      DataGridView dgv = sender as DataGridView;
      if (dgv == null) { return; }
      dgv.Refresh();
      if (dgv.CurrentRow.Selected)
      {
      //  //bndngSrc.EndEdit();
      //  SendKeys.Send("^~");
        return;
      }
      bool cv;
      if (!bool.TryParse(dgv.CurrentCell.Value.ToString(), out cv)) { cv = false; }

      if (dgv.CurrentCell.ColumnIndex.Equals(0) && e.RowIndex != -1 && cv)
      {
        DataRowView drv = (DataRowView)dgv.CurrentRow.DataBoundItem;
        drv["Template File"] = string.Empty;
        dgv.Refresh();
        SendKeys.Send("^~");
        return;
      }
      if (dgv.CurrentCell.ColumnIndex.Equals(0) && e.RowIndex != -1 && !cv)
      {
        DataRowView drv = (DataRowView)dgv.CurrentRow.DataBoundItem;
        if (drv["ARAN"] != DBNull.Value && !string.IsNullOrEmpty(drv["ARAN"].ToString()))
        {
          drv["Template File"] = txtBxTemplateFile.Text;
        }
        else
        {
          MessageBox.Show("Can not process this batch, missing ARAN Id.", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        dgv.Refresh();
        SendKeys.Send("^~");
        return;
      }
    }

    private void dtaGrdVwBatchDt_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      DataGridView dgv = sender as DataGridView;
      if (dgv == null) { return; }
      dgv.Refresh();
      bool cv;
      if (!bool.TryParse(dgv.CurrentCell.Value.ToString(), out cv)) { cv = false; }

      DataRowView drv = (DataRowView)dgv.CurrentRow.DataBoundItem;
      if (dgv.CurrentCell.ColumnIndex.Equals(0) && e.RowIndex != -1 &&
          cv &&
          (drv["ARAN"] == DBNull.Value || string.IsNullOrEmpty(drv["ARAN"].ToString())))
      {
        drv["Process"] = false;
        dgv.Refresh();
      }
    }
    
    private void dtTmPkrStartDateAndTime_ValueChanged(object sender, EventArgs e)
    {
      lblStatus.Text = string.Empty;
      lblStatus.Update();
    }

    private void frmSBETDialog_Shown(object sender, EventArgs e)
    {
      MessageBox.Show("Click \"Refresh Projects\" and use your web browser to save a list of the most current projects needing SBET batch processing",
                "Action Needed!!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
    }

    private List<FileInfo> GetCSVFiles()//2.1
    {
      string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
      string pathDwnld = Path.Combine(pathUser, "Downloads");
      return new DirectoryInfo(pathDwnld).GetFiles("*.csv").OrderByDescending(f => f.LastWriteTime).ToList();
    }

    private string GetNewComment(string exstCmnt, string addCmnt)
    {
      if (string.IsNullOrEmpty(addCmnt)) { return exstCmnt; }
      if (string.IsNullOrEmpty(exstCmnt)) { return addCmnt; }
      else { return string.Format("{0}; {1}", exstCmnt, addCmnt); }
    }

    private string GetPathOrFileName(string nm)
    {
      string fltr = string.Format("[Name] = '{0}'", nm);
      DataRow[] fNfDr = fNfDt.Select(fltr);
      if (fNfDr.Length > 0)
      {
        return fNfDr[0]["PathOrFileName"].ToString();
      }
      else
      {
        return string.Empty;
      }
    }

    private void InitializeControls() //1
    {
      Version ver = Assembly.GetExecutingAssembly().GetName().Version;
      this.Text = string.Format("{0}    ver. {1}.{2}.{3}.{4}",
                                this.Text,
                                ver.Major,
                                ver.Minor.ToString("00"),
                                ver.Build.ToString("00"),
                                ver.Revision.ToString("00"));

      rdoBtnGoogleChrome.Checked = true;
      Common cmn = new Common();
      string fNfXMLFile = Path.Combine(cmn.UserDataFolder(), xmlFilesAndFoldersNm);
      if (!File.Exists(fNfXMLFile))
      {
        string srcfNfXMLFile = Path.Combine(Path.Combine(cmn.GetRootDirectory(),"Data"), xmlFilesAndFoldersNm);
        File.Copy(srcfNfXMLFile, fNfXMLFile);
      }
      fNfDs = new DataSet();
      fNfDs.ReadXml(fNfXMLFile);
      fNfDt = fNfDs.Tables["FolderOrFile"];

      fNfSBETPOSPacExportPy = Path.Combine(cmn.GetRootDirectory(), GetPathOrFileName("POSPacExport"));
      posPacExe = GetPathOrFileName("POSPacMMS");
      pyExeFlNm = GetPathOrFileName("Python");
      //gets directories

      CreateDataTableStructures();
      //LoadProjects();
      LoadLocalFolderForProjects();
      LoadServerFolderForProjects();
      LoadTemplateFile();
      LoadGNSSModes(ref cmn);

      lblStatus.Text = string.Empty;
      lblStatus.Update();
    }

    private bool IsBatchFileSelected()//5.x
    {
      DataRow[] dr = batchDt.Select("[Process] = True");
      if (dr.Length > 0)
      {
        return true;
      }
      else
      {
        MessageBox.Show("A project batch file must be checked before the batch process can be started.", 
                        "ERROR!!!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        return false;
      }
    }

    private bool IsProjectLoaded()//5.x
    {
      if (cmboBxProjects.Items.Count > 0)
      {
        return true;
      }
      else
      {
        MessageBox.Show("Projects must be loaded before the batch process can be started.", 
                        "ERROR!!!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        return false;
      }
    }

    private void LoadBatches()//2.2, 5.1.3
    {
      DataView dv = batchDt.DefaultView;
      dv.RowFilter = string.Format("[Project Name] = '{0}' AND [Database Name] IS NOT NULL AND [Database Name] <> '' AND [Database Name] <> '-'", cmboBxProjects.SelectedValue);
      dv.Sort = "[Batch Name]";
      bndngSrc.DataSource = dv;
      bndngNav.BindingSource = bndngSrc;
      dtaGrdVwBatchDt.DataSource = bndngSrc;
      dtaGrdVwBatchDt.Columns["Project Name"].Visible = false;
      dtaGrdVwBatchDt.Columns["Video Location"].Visible = false;
      dtaGrdVwBatchDt.Columns["Database Name"].Visible = false;
      foreach (DataGridViewColumn dgvc in dtaGrdVwBatchDt.Columns)
      {
        dgvc.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12.0F, GraphicsUnit.Point);
      }

      dtaGrdVwBatchDt.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
      dtaGrdVwBatchDt.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
      dtaGrdVwBatchDt.AllowUserToAddRows = false;
      dtaGrdVwBatchDt.AllowUserToDeleteRows = false;
      //dtaGrdVwBatchDt.Columns["Project Name"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["Batch Name"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["ARAN"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["Data Collection From Date"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["Data Collection To Date"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["Template File"].ReadOnly = true;
      dtaGrdVwBatchDt.Columns["Comments"].ReadOnly = true;
    }

    private void LoadGNSSModes(ref Common cmn)//1.4
    {
      string gnssModesXMLFile = Path.Combine(cmn.GetRootDirectory(), xmlGNSSModesNm);
      DataSet gnssDs = new DataSet();
      gnssDs.ReadXml(gnssModesXMLFile);
      DataTable gnssDt = gnssDs.Tables["GNSSMode"];
      gnssDt.DefaultView.Sort = "SeqOrder";
      cmboBxGNSSMode.Items.Clear();
      cmboBxGNSSMode.DataSource = gnssDt;
      cmboBxGNSSMode.DisplayMember = "DisplayName";
      cmboBxGNSSMode.ValueMember = "Name";
    }

    private void LoadLocalFolderForProjects()//1.2
    {
      txtBxLocalFolder.Text = GetPathOrFileName("LocalProjectFolder");
      strLocalProjectFolder = txtBxLocalFolder.Text;
    }

    private void LoadProjects() //2
    {
      lblStatus.Text = "Loading projects.";
      lblStatus.Update();
      string slctn = string.Empty;
      if (cmboBxProjects.Items.Count > 0) { slctn = cmboBxProjects.SelectedValue.ToString(); }
      string flNm = string.Empty;
      if (rdoBtnInternetExplorer.Checked)
      {
        flNm = GetPathOrFileName("InternetExplorer");
      }
      else
      {
        flNm = GetPathOrFileName("GoogleChrome");
      }
      string url = GetPathOrFileName("Projects");

      List<FileInfo> csvFls = GetCSVFiles();
      int n = csvFls.Count;
        //list of fileinfos on all .csvs in downloads folder

      Process p = new Process();
      p.StartInfo = new ProcessStartInfo(flNm);
      p.StartInfo.Arguments = string.Format("\"{0}\"", url);
      p.Start();
      p.Close();
        //opens browser, goes to url, closes after operation complete

      int cnt = 0;
      while (csvFls.Count == n && cnt < 150)
      {
        csvFls = GetCSVFiles();
        Thread.Sleep(2000);
        cnt++;
      }
        //wait until the # of csv files in folder increases

      if (cnt < 150)
      {
        projDt.Clear();
        string[] csvRows = File.ReadAllLines(csvFls[0].FullName); //read latest csv?
        n = 0;
        foreach (string csvRow in csvRows)
        {
          if (string.IsNullOrEmpty(csvRow)) { break; }
          if (n > 0)
          {
            string[] qs = csvRow.Split('"');
            for (int i = 0; i < qs.Length; i++)
            {
              if (qs[i] == ",") { qs[i] = "~"; }
            }
            DataRow dr = projDt.NewRow();
            //dr.ItemArray = csvRow.Replace("\"", string.Empty).Split(',');
            dr.ItemArray = string.Join("\"", qs).Replace("\"", string.Empty).Split('~');
            projDt.Rows.Add(dr);
          }
          n++;
        } //put .csv data into projDt
        DataTable dt = new DataTable();
        dt = projDt.DefaultView.ToTable(true, "Project Name");
        dt.DefaultView.Sort = "[Project Name]";
        cmboBxProjects.DataSource = null;
        cmboBxProjects.Items.Clear();
        cmboBxProjects.DataSource = dt;
        cmboBxProjects.DisplayMember = "Project Name";
        cmboBxProjects.ValueMember = "Project Name";
        File.Delete(csvFls[0].FullName);

        batchDt = projDt.Clone();
        batchDt.Clear();
        batchDt.Merge(projDt);
        batchDt.Columns.Remove("GPS Post-Processing Imported Date");
        batchDt.Columns.Remove("GPS Post-Processing Required Date");
        batchDt.Columns.Remove("GPS Post-Processing Technician");
        batchDt.Columns.Remove("Server Name");
        //batchDt.Columns.Remove("Database Name");
        batchDt.Columns.Add("Comments", typeof(System.String));
        batchDt.Columns.Add("Template File", typeof(System.String));
        batchDt.Columns.Add("Process", typeof(System.Boolean)).SetOrdinal(0);
        foreach (DataRow dr in batchDt.Rows)
        {
          if (dr["ARAN"] == DBNull.Value || string.IsNullOrEmpty(dr["ARAN"].ToString()))
          {
            dr["Comments"] = GetNewComment(dr["Comments"].ToString(), "ARAN Id is missing, cannot process SBET data.");
          }
          //if (dr["Video Location"] == DBNull.Value || string.IsNullOrEmpty(dr["Video Location"].ToString()) ||
          //    !Directory.Exists(dr["Video Location"].ToString()))
          //{
          //  dr["Comments"] = GetNewComment(dr["Comments"].ToString(), "Video location is invalid, cannot process SBET data.");
          //}
          dr["Database Name"] = dr["Database Name"].ToString().Trim();
          if (dr["Database Name"] == DBNull.Value || string.IsNullOrEmpty(dr["Database Name"].ToString()))
          {
            dr["Comments"] = GetNewComment(dr["Comments"].ToString(), "Databse name is missing, cannot process SBET data.");
          }
        }
        if (!string.IsNullOrEmpty(slctn)) { cmboBxProjects.SelectedValue = slctn; }
        LoadBatches();
        //lblStatus.Text = string.Format("Projects loaded. {0} projects, {1} batches",projDt.Rows.Count, batchDt.Rows.Count);
        lblStatus.Text = "Projects loaded.";
        lblStatus.Update();
      }
      else //timeout on download
      {
        cmboBxProjects.DataSource = null;
        cmboBxProjects.Items.Clear();
        lblStatus.Text = "No projects were loaded.";
        lblStatus.Update();
      }

    }

    private void LoadServerFolderForProjects()
    {
      txtBxServerFolder.Text = GetPathOrFileName("ServerProjectFolder");
      strServerProjectFolder = txtBxServerFolder.Text;
    }

    private void LoadTemplateFile()//1.3
    {
      string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
      txtBxTemplateFile.Text = Path.Combine(pathUser, GetPathOrFileName("TemplateFile"));
      strTemplateFile = txtBxTemplateFile.Text;
    }

    private void POSPacProcess(string posPacExe, string btchPrjFlNm)
    {
      Process p = new Process();
      p.StartInfo = new ProcessStartInfo(posPacExe);
      p.StartInfo.Arguments = string.Format("-b \"{0}\"", btchPrjFlNm);
      p.Start();
      p.WaitForExit();
      p.Close();
      p.Dispose();
    }

    private void ProcessBatches(bool skpPosPacProcess)
    {
      //Loop through posbat files
      DataRow[] drs = batchDt.Select("[Process] = True", "[Project Name], [Batch Name]");
      foreach (DataRow dr in drs)
      {
        string posBatFlNm = string.Format("{0}_{1}.posbat", dr["Database Name"].ToString(), dr["Batch Name"].ToString());
        string btchPrjFullFlNm = Path.Combine(strLocalProjectFolder, posBatFlNm);
        if (File.Exists(btchPrjFullFlNm))
        {
          if (!skpPosPacProcess)
          {
            //lblStatus.Text = string.Format("Processing {0}. {1} processes, {2} projects", posBatFlNm, drs.Length, batchDt.Rows.Count);
            lblStatus.Text = string.Format("Processing {0}.", posBatFlNm);
            lblStatus.Update();
            POSPacProcess(posPacExe, btchPrjFullFlNm);
            //lblStatus.Text = string.Format("{0} processed.  {1} processes, {2} projects", posBatFlNm, drs.Length, batchDt.Rows.Count);
            lblStatus.Text = string.Format("{0} processed.", posBatFlNm);
            lblStatus.Update();
          }

          lblStatus.Text = string.Format("Post processing {0}.", posBatFlNm);
          lblStatus.Update();
          //MessageBox.Show(string.Format("Sending \"{0}\" to PostProcessBatchMissions.", btchPrjFlNm));
          PostProcessBatchMissions ppbm = new PostProcessBatchMissions(btchPrjFullFlNm);

          if (!ppbm.HasMissionAborted())
          {
            lblStatus.Text = string.Format("{0} post processed.", posBatFlNm);
          }
          else
          {
            lblStatus.Text = string.Format("{0} aborted.", posBatFlNm);
            dr["Comments"] = string.Format("*** ABORTED ***;{0}",dr["Comments"].ToString());
          }
          lblStatus.Update();

          //string posBadBatFlNm = ppbm.GetBadProjectFileName();
          //if (!string.IsNullOrEmpty(posBadBatFlNm))
          //{
          //  lblStatus.Text = string.Format("Processing {0}.", posBadBatFlNm);
          //  lblStatus.Update();
          //  POSPacProcess(posPacExe, posBadBatFlNm);
          //  lblStatus.Text = string.Format("{0} processed.", posBadBatFlNm);
          //  lblStatus.Update();
          //}

          //Block If replaced with export tag in the posbat xml file
          //if (!ppbm.HasMissionAborted() && rdoBtnBatchAndExport.Checked)
          //{

          //  lblStatus.Text = string.Format("Exporting {0}.", posBatFlNm);
          //  lblStatus.Update();
          //  string fldrNm = string.Format("{0}\\{1}",
          //                                           txtBxLocalFolder.Text,
          //                                           Path.GetFileNameWithoutExtension(posBatFlNm));
          //  ExportBatchMissions exBtMsn = new ExportBatchMissions(pyExeFlNm,
          //                                                        fNfSBETPOSPacExportPy,
          //                                                        fldrNm,
          //                                                        posPacExe, dr["Video Location"].ToString(),
          //                                                        dr["Batch Name"].ToString());
          //  lblStatus.Text = string.Format("{0} exported.", posBatFlNm);
          //  lblStatus.Update();
          //}

        }
      }
    }

    private void rdoBtnGoogleChrome_CheckedChanged(object sender, EventArgs e)
    {
      lblStatus.Text = string.Empty;
      lblStatus.Update();
    }

    private void rdoBtnInternetExplorer_CheckedChanged(object sender, EventArgs e)
    {
      lblStatus.Text = string.Empty;
      lblStatus.Update();
    }

    private void SaveFolderOrFile(string nm, string pthOrFlNm)
    {
      string fltr = string.Format("[Name] = '{0}'", nm);
      DataRow[] dr = fNfDt.Select(fltr);

      if (dr.Length > 0)
      {
        //  string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        //  if (pthOrFlNm.Contains(pathUser))
        //  {
        //    dr[0]["PathOrFileName"] = pthOrFlNm.Substring(pathUser.Length + 1);
        //  }
        //  else
        //  {
            dr[0]["PathOrFileName"] = pthOrFlNm;
        //  }

        Common cmn = new Common();
        string fldrsNFlsXMLFile = Path.Combine(cmn.UserDataFolder(), xmlFilesAndFoldersNm);
        fNfDs.WriteXml(fldrsNFlsXMLFile);
      }
    }

    private void SetupBatchesForProcessing()//5.1 //copy pcs files into own folder and take away skipped
    {
      DataRow[] drs = batchDt.Select("[Process] = True", "[Project Name], [Batch Name]");
      foreach (DataRow dr in drs)
      {
        //Create Project folder(s)
        string dbFldrNm = Path.Combine(strLocalProjectFolder, dr["Database Name"].ToString());
        //if (!Directory.Exists(prjctFldrNm)) { Directory.CreateDirectory(prjctFldrNm); }

        //Create ARAN folder(s)
        string aranFldrNm = string.Empty;
        aranFldrNm = Path.Combine(dbFldrNm, dr["ARAN"].ToString());
        //if (!Directory.Exists(aranFldrNm)) { Directory.CreateDirectory(aranFldrNm); }

        //Create Batch folder(s)
        string batchFldrNm = Path.Combine(aranFldrNm, dr["Batch Name"].ToString());
        //if (!Directory.Exists(batchFldrNm)) { Directory.CreateDirectory(batchFldrNm); }

        //Create PCS folder(s)
        //string pcsFldrNm = Path.Combine(batchFldrNm, "PCS");
        //if (!Directory.Exists(pcsFldrNm)) { Directory.CreateDirectory(pcsFldrNm); }
        string pcsFldrNm = Path.Combine(strServerProjectFolder, dr["Database Name"].ToString());
        if (!Directory.Exists(pcsFldrNm)) { Directory.CreateDirectory(pcsFldrNm); }

        pcsFldrNm = Path.Combine(pcsFldrNm, dr["ARAN"].ToString());
        if (!Directory.Exists(pcsFldrNm)) { Directory.CreateDirectory(pcsFldrNm); }
        aranFldrNm = pcsFldrNm;

        pcsFldrNm = Path.Combine(pcsFldrNm, dr["Batch Name"].ToString());
        if (!Directory.Exists(pcsFldrNm)) { Directory.CreateDirectory(pcsFldrNm); }

        pcsFldrNm = Path.Combine(pcsFldrNm, "PCS");
        if (!Directory.Exists(pcsFldrNm)) { Directory.CreateDirectory(pcsFldrNm); }

        //Migrate POS Files
        //string[] fldrPrts = dr["Video Location"].ToString().Split('\\');
        //fldrPrts[2] = string.Format("{0}.Roadware.com", fldrPrts[2]);
        //MigratePOSFiles mpf = new MigratePOSFiles(aranFldrNm,
        //                                          batchFldrNm,
        //                                          string.Join("\\", fldrPrts),
        //                                          dr["Data Collection From Date"].ToString(),
        //                                          dr["Data Collection To Date"].ToString());

        MigratePOSFiles mpf;

        //if (rdoBtnFolderPost14.Checked)
        //{
            //migrate for new posdata structure 2014
            mpf = new MigratePOSFiles(aranFldrNm,
                                      batchFldrNm,
                                      dr["Video Location"].ToString(),
                                      dr["Data Collection From Date"].ToString(),
                                      dr["Data Collection To Date"].ToString(),
                                      dr["ARAN"].ToString(),
                                      pcsFldrNm);
        //}

        //else
        //{
        //    mpf = new MigratePOSFiles(aranFldrNm,
        //                                              batchFldrNm,
        //                                              dr["Video Location"].ToString(),
        //                                              dr["Data Collection From Date"].ToString(),
        //                                              dr["Data Collection To Date"].ToString(),
        //                                              pcsFldrNm);
        //    //migrate pos/pcs files 5.1.1->new class
        //}

        dr["Comments"] = mpf.GetComments();
        SetupBatchMissions sbm = new SetupBatchMissions(pcsFldrNm, cmboBxGNSSMode.SelectedValue.ToString(), strTemplateFile);
          //migrate pos/pcs files 5.1.2->new class

          prjcts = sbm.GetProjects();

        if (!string.IsNullOrEmpty(prjcts))
        {
          WriteBatchProcessFile(string.Format("{0}_{1}", dr["Database Name"].ToString(), dr["Batch Name"].ToString()));
        }
      }
      LoadBatches();
    }

    private void tmrStartDateAndTime_Tick(object sender, EventArgs e)
    {
      if (timeLeft.Days > 0 || timeLeft.Hours > 0 || timeLeft.Minutes > 0 || timeLeft.Seconds > 0)
      {
        TimeSpan second = TimeSpan.FromSeconds(1);
        timeLeft = timeLeft.Subtract(second);
        if (timeLeft.Days > 0)
        {
          lblStatus.Text = string.Format("{0} days {1}:{2}:{3} remaining before process starts.",
                                         timeLeft.Days,
                                         timeLeft.Hours.ToString("00"),
                                         timeLeft.Minutes.ToString("00"),
                                         timeLeft.Seconds.ToString("00"));
        }
        else
        {
          lblStatus.Text = string.Format("{0}:{1}:{2} remaining before process starts.",
                                         timeLeft.Hours.ToString("00"),
                                         timeLeft.Minutes.ToString("00"),
                                         timeLeft.Seconds.ToString("00"));
        }
        lblStatus.Update();
      }
      else
      {
        tmrStartDateAndTime.Stop();
        btnStartBatchProcess.Enabled = false;
        ProcessBatches(false);
        btnStartBatchProcess.Enabled = true;
        ChangeControlStatus(true);
        btnStartBatchProcess.Text = "Start Batch Process";
        lblStatus.Text = "Processing Completed!!";
        //lblStatus.Text = string.Format("Use the batch xml file ({0}) to load POSPac projects.", btchPrjFlNm);
        lblStatus.Update();
        Cursor.Current = Cursors.Default;
      }
    }

    private void UpdateTextBoxChanges(ref TextBox txtBx, ref string strCmpr, string fldNm) //why "ref"s
    {
      if (txtBx.Text != strCmpr)
      {
        strCmpr = txtBx.Text;
        SaveFolderOrFile(fldNm, strCmpr);
      }
    }

    private void WriteBatchProcessFile(string posBatFlNm)
    {
      Common cmn = new Common();
      string btch = cmn.GetTags("Batch");
      btch = btch.Replace("##Projects##", prjcts);
      string btchPrjFullFlNm = Path.Combine(strLocalProjectFolder, string.Format("{0}.posbat",posBatFlNm));
      using (StreamWriter sw = new StreamWriter(btchPrjFullFlNm))
      {
        sw.Write(btch);
      }
      string xmlPrjFlNm = Path.Combine(strLocalProjectFolder, string.Format("{0}.xml", posBatFlNm));
      fNfDs.WriteXml(xmlPrjFlNm);
    }
    #endregion

    private void btnTest_Click(object sender, EventArgs e)
    {
      //DataRow[] dr = batchDt.Select("[Process] = True");
      //for (int i = 0; i < dr.Length; i++)
      //{
      //  //dr[i]["Comments"] = "Selected";
      //  Debug.WriteLine(dr[i]["Template File"].ToString());
      //}
      ProcessBatches(true);
    }

  }

}
