﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using RoadwareSBETClassLibrary.Domain;

namespace RoadwareSBETClassLibrary
{
  public class Test
  {
    //RoadwareSBETClassLibrary.Test.Dwnld()
    public static void Dwnld()
    {
      string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
      string pathDwnld = Path.Combine(pathUser, "Downloads");
      Debug.WriteLine(pathDwnld);
      List<FileInfo> sortedFiles = new DirectoryInfo(pathDwnld).GetFiles("*.csv").OrderByDescending(f => f.LastWriteTime).ToList();
      foreach (FileInfo fi in sortedFiles)
      {
        Debug.WriteLine(string.Format("{0} {1}",fi.Name,fi.LastWriteTime));
      }


    }

    //RoadwareSBETClassLibrary.Test.WebReq()
    public static void WebReq()
    {
      //string url = "https://fugroroadware.my.salesforce.com/?ec=302&startURL=%2F00Oa000000812Vf%3Fexport%3D1%26pv0%3D%2527Virginia%2B2014%2527%26xf%3Dcsv%26enc%3DUTF-8";
      //string url = "https://fugroroadware.my.salesforce.com/00Oa000000812Vf?pv0=%27Virginia%202014%27&export=1&enc=UTF-8&xf=csv";
      //string url = "https://fugroroadware--c.na13.content.force.com/secur/contentDoor?startURL=https%3A%2F%2Ffugroroadware.my.salesforce.com%2F00Oa000000812Vf%3Fexport%3D1%26pv0%3D%2527Virginia%2B2014%2527%26xf%3Dcsv%26enc%3DUTF-8&sid=00D30000000mwtN%21ARkAQEDo0yzlS6rH.0ALLRbC0wJlaq4hs6pXN9UZYheEDioL_aZ6T28cOezTVIBLmSoiaedXO7es.zYAt6IE8h0bOk9ta1Xs&skipRedirect=1&lm=aLo1sD8ASUm1tFkUqemqwg%3D%3D";
      //string url = "https://fugroroadware--c.na13.content.force.com/secur/contentDoor?startURL=https%3A%2F%2Ffugroroadware.my.salesforce.com%2F00Oa000000818SS%3Fexport%3D1%26xf%3Dcsv%26enc%3DUTF-8&sid=00D30000000mwtN%21ARkAQDkHKUIeBWUJEwHRHM9XwCuCzgHHLJnfbu8_mZez5JqnswrHYVQPzXcj_s2idR0DqW31W8FBtM0PMEUvTHF5IwTQ5cPl&skipRedirect=1&lm=aLo1sD8ASUm1tFkUqemqwg%3D%3D";
      string url = "https://fugroroadware.my.salesforce.com/00Oa00000081BUC?export=1&enc=UTF-8&xf=csv";
      /*
      WebRequest wReq = WebRequest.Create(url);
      wReq.Credentials = CredentialCache.DefaultCredentials;
      WebResponse wRsp = wReq.GetResponse();

      StreamReader sRdr = new StreamReader(wRsp.GetResponseStream());
      string resFrmSrvr = sRdr.ReadToEnd();
      Debug.WriteLine(resFrmSrvr);
      sRdr.Close();
      wRsp.Close();
      */
      Process p = new Process();
      //p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe");
      p.StartInfo = new ProcessStartInfo(@"C:\Program Files\Internet Explorer\iexplore.exe");
      p.StartInfo.Arguments = string.Format("\"{0}\"",url);
      p.Start();
      //p.WaitForExit();
      
    }

    //RoadwareSBETClassLibrary.Test.CSVFileRead()
    public static void CSVFileRead()
    {
      string[] csvRows = File.ReadAllLines(@"C:\Users\coxc\Downloads\report1394558580886.csv");
      int n = 0;
      foreach (string csvRow in csvRows)
      {
        if (string.IsNullOrEmpty(csvRow)) { break; }
        if (n > 0)
        {
          Debug.WriteLine(csvRow.Replace("\"", string.Empty));
        }
        n++;
      }

    }

    //RoadwareSBETClassLibrary.Test.ListDirectories()
    public static void ListDirectories()
    {
      string dtaFldr = @"\\video-12.roadware.com\V12A\85006LA13_Locals\1730\Posdata";
      foreach (string fldr in Directory.GetDirectories(dtaFldr).OrderBy(f => f).ToArray())
      {
        Debug.WriteLine(fldr);
      }
    }

    //RoadwareSBETClassLibrary.Test.Migration()
    public static void Migration()
    {
      //string localAranIdFolderNm = @"C:\Repository\Roadware\SBETProcessing\Projects\Louisiana Locals 2013B\ARAN 40";
      //string localBatcFolderNm = @"C:\Repository\Roadware\SBETProcessing\Projects\Louisiana Locals 2013B\ARAN 40\CTRL-19";
      //string serverVideoLocationFolderNm = @"\\Video-12.Roadware.com\V12A\85006LA13_Locals\1730";
      //string dataCollectionFromDate = "2/3/2014";
      //string dataCollectionToDate = "2/4/2014";
      //string serverPcsFolderNm = @"C:\Repository\Roadware\SBETProcessing\Projects\Louisiana Locals 2013B\ARAN 40\CTRL-19\PCS";
      //MigratePOSFiles mpf = new MigratePOSFiles(localAranIdFolderNm, 
      //                                          localBatcFolderNm, 
      //                                          serverVideoLocationFolderNm, 
      //                                          dataCollectionFromDate, 
      //                                          dataCollectionToDate,
      //                                          serverPcsFolderNm);

      string localAranIdFolderNm = @"C:\Temp\Roadware\SBET\Test\Migrate\85045IRV15_ARAN49_Controls\ARAN 49";
      string localBatcFolderNm = @"C:\Temp\Roadware\SBET\Test\Migrate\85045IRV15_ARAN49_Controls\ARAN 49\CTRL-07";
      //string serverVideoLocationFolderNm = @"\\Video-13\V13A\85045IRV15";
      string serverVideoLocationFolderNm = @"\\Video-13.roadware.com\V13A\85045IRV15";
      string dataCollectionFromDate = "11/3/2015";
      string dataCollectionToDate = "12/3/2015";
      string ARAN = "ARAN 49";
      string pcsFldrNm = @"C:\Temp\Roadware\SBET\Test\Server\85045IRV15_ARAN49_Controls\ARAN 49\CTRL-07\PCS";

      MigratePOSFiles mpf = new MigratePOSFiles(localAranIdFolderNm,
                                                localBatcFolderNm,
                                                serverVideoLocationFolderNm,
                                                dataCollectionFromDate,
                                                dataCollectionToDate,
                                                ARAN,
                                                pcsFldrNm);

    }

    //RoadwareSBETClassLibrary.Test.PostProcess()
    public static void PostProcess()
    {
      //string btchPrjFlNm = @"C:\Repository\Roadware\SBETProcessing\Projects\Virginia 2014_50.posbat";
      string btchPrjFlNm = @"C:\Temp\Roadware\SBET\Test\PostProcess\City of Irving - Pavement Management Implementation_CTRL-07.posbat";
      PostProcessBatchMissions pbm = new PostProcessBatchMissions(btchPrjFlNm);
    }

    //RoadwareSBETClassLibrary.Test.PosBatFileRead()
    public static void PosBatFileRead()
    {
      //string posBatFlNm = @"C:\Repository\Roadware\SBETProcessing\Louisiana Locals 2013B_09.posbat";
      string posBatFlNm = @"C:\Repository\Roadware\SBETProcessing\City of Irving - Pavement Management Implementation_CTRL-07.posbat";
      Common cmn = new Common();
      cmn.GetServerFolderFromPosBatFile(posBatFlNm);
    }

    //RoadwareSBETClassLibrary.Test.PosBatFileProject()
    public static void PosBatFileProject()
    {
      string posBatFlNm = @"C:\Repository\Roadware\SBETProcessing\City of Irving - Pavement Management Implementation_CTRL-07.posbat";
      //DataSet posBatDs = new DataSet();
      //posBatDs.ReadXml(posBatFlNm);
      //DataTable extractDt = posBatDs.Tables["Project"];
      //Debug.WriteLine(string.Format("Project_id = {0}", extractDt.Rows[1]["Project_Id"].ToString()));
      //DataTable sbDt = posBatDs.Tables["SmartBase"];

      Temp tmp = new Temp(posBatFlNm);

    }

    //RoadwareSBETClassLibrary.Test.ServerFolderFromPosBatFile()
    public static void ServerFolderFromPosBatFile()
    {
      string btchPrjFullFlNm = @"C:\Temp\Roadware\SBET\Louisiana Locals C - July 1,2014 to October 20,2016_82.posbat";
      Common cmn = new Common();
      Debug.WriteLine(cmn.GetServerFolderFromPosBatFile(btchPrjFullFlNm));
    }

    //RoadwareSBETClassLibrary.Test.BadFind()
    public static void BadFind()
    {
      string prjNmBatNm = "City of Irving - Pavement Management Implementation_CTRL-07";
      //string prjNmBatNm = "City of Irving - Pavement Management Implementation_CTRL-07_BAD";

      if (prjNmBatNm.Substring(prjNmBatNm.Length - 3, 3).ToUpper() != "BAD")
      {
        Debug.WriteLine(string.Format("{0} is not Bad!!", prjNmBatNm.Substring(prjNmBatNm.Length - 3, 3).ToUpper()));
      }
      else
      {
        Debug.WriteLine(string.Format("{0} is Bad!!", prjNmBatNm.Substring(prjNmBatNm.Length - 3, 3).ToUpper()));
      }

    }

  }
}
