﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RoadwareSBETClassLibrary.Domain
{
  public class Common
  {
    #region fields

    #endregion

    #region constructors
    public Common() { }
    #endregion

    #region methods
    public Guid AppGuid()
    {
      Assembly asm = Assembly.GetEntryAssembly();
      object[] attr = asm.GetCustomAttributes(typeof(GuidAttribute), true);
      return new Guid((attr[0] as GuidAttribute).Value);
    }

    public string CheckFolder(string flFldr)
    {
      if (!Directory.Exists(flFldr)) { Directory.CreateDirectory(flFldr); }
      return flFldr;
    }

    public DataTable CreateNewDataTable(List<DataTableField> dataTableFields, string dataTableName)
    {
      DataTable dt = new DataTable(dataTableName);
      foreach (DataTableField dtFld in dataTableFields)
      {
        DataColumn dtCol = new DataColumn(dtFld.Name, dtFld.Type);
        dt.Columns.Add(dtCol);
      }
      return dt;
    }

    public void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true, bool removeSourceDir = false)
    {
      DirectoryInfo di = new DirectoryInfo(sourceDirName);
      DirectoryInfo[] dis = di.GetDirectories();
      if (di.Exists)
      {
        if (!Directory.Exists(destDirName)) { Directory.CreateDirectory(destDirName); }
        FileInfo[] fis = di.GetFiles();
        foreach (FileInfo fi in fis)
        {
          fi.CopyTo(Path.Combine(destDirName, fi.Name), true);
        }
        if (copySubDirs)
        {
          foreach (DirectoryInfo sdi in dis)
          {
            DirectoryCopy(sdi.FullName, Path.Combine(destDirName, sdi.Name), copySubDirs, removeSourceDir);
          }
        }
        if (removeSourceDir)
        {
          Directory.Delete(sourceDirName, true);
        }
      }
      else
      {
        MessageBox.Show(string.Format("Could not find source folder, {0}, to copy contents.", sourceDirName), "ERROR!!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
      }
    }

    public string GetProcessTags(string gnssMd)
    {
      string processFlNm = Path.Combine(GetRootDirectory(), string.Format("Data\\Process{0}.txt", gnssMd));
      string processTags = string.Empty;
      using (StreamReader sr = new StreamReader(processFlNm))
      {
        processTags = sr.ReadToEnd();
      }
      return processTags;
    }

    public string GetRootDirectory()
    {
      string curDirectory = AppDomain.CurrentDomain.BaseDirectory;
      string retDirectory = curDirectory;
      if (curDirectory.IndexOf("obj") >= 0)
      {
        List<string> rootPrts = curDirectory.Split('\\').ToList<string>();
        int n = rootPrts.Count - 4;
        rootPrts.RemoveRange(n, 4);
        retDirectory = string.Join("\\", rootPrts.ToArray());
      }

      return retDirectory;
    }

    public string GetServerFolderFromPosBatFile(string posBatFullFlNm)
    {
      DataSet posBatDs = new DataSet();
      posBatDs.ReadXml(posBatFullFlNm);
      DataTable extractDt = posBatDs.Tables["Extract"];
      string svrFldrNm = string.Empty;
      if (extractDt.Rows.Count > 0)
      {
        string posFullFlNm = extractDt.Rows[0]["FirstPosFile"].ToString().ToUpper();
        string[] posFullFlNmPrts = posFullFlNm.Split('\\');
        if (posFullFlNmPrts.Length > 4)
        {
          Array.Resize(ref posFullFlNmPrts, posFullFlNmPrts.Length - 5);
          svrFldrNm = string.Join("\\", posFullFlNmPrts);
        }
      }
      return svrFldrNm;
    }

    public string GetTags(string tagNm)
    {
      string tagFlNm = Path.Combine(GetRootDirectory(), string.Format("Data\\{0}.txt", tagNm));
      string tags = string.Empty;
      using (StreamReader sr = new StreamReader(tagFlNm))
      {
        tags = sr.ReadToEnd();
      }
      return tags;
    }

    public string UserDataFolder()
    {
      Guid appGuid = AppGuid();
      string fldrBse = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    @"Roadware\SBET");
      string fldr = Path.Combine(fldrBse, appGuid.ToString("B").ToUpper());
      return CheckFolder(fldr);
    }
    #endregion
  }

  public class DataTableField
  {
    #region properties
    public string Name { get; set; }
    public Type Type { get; set; }
    #endregion
  }
}
