﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadwareSBETClassLibrary.Domain
{
  public class SetupBatchMissions
  {
    #region fields
    private DataTable missionsDt;

    List<FileInfo> posFlNms;

    private string btchPCSFldrNm;
    private string gnssMd;
    private string lstFlNm;
    private string missionsDtNm = "Missions";
    private string prjcts=string.Empty;
    private string srcRds = "0";
    private string stpTmTotSec = "999999";
    private string strtTmTotSec = "0";
    private string tmpltFlNm;
    #endregion

    #region constructors
    public SetupBatchMissions(string batchPCSFolderName, string GNSSMode, string templateFileName)
    {
      btchPCSFldrNm = batchPCSFolderName;
      gnssMd = GNSSMode;
      tmpltFlNm = templateFileName;
      SetupTheBatchMissions();
    }
    #endregion

    #region methods
    private void AddBatchProjectsFromMissions()//3
    {
      Common cmn = new Common();
      foreach (DataRow dr in missionsDt.Rows)
      {
        //Project
        string prj = cmn.GetTags("Project");
        prj = prj.Replace("##ProjectName##", dr["Project Name"].ToString());

        //Extract
        string xtrct = cmn.GetTags("Extract");
        xtrct = xtrct.Replace("##FirstPosFileName##", dr["First POS File"].ToString());
        xtrct = xtrct.Replace("##LastPosFileName##", dr["Last POS File"].ToString());
        xtrct = xtrct.Replace("##StartTimeTotalSeconds##", strtTmTotSec);
        xtrct = xtrct.Replace("##StopTimeTotalSeconds##", stpTmTotSec);
        prj = prj.Replace("##Extract##", xtrct);

        //Process
        string prcs = cmn.GetProcessTags(gnssMd);
        prcs = prcs.Replace("##SearchRadius##", srcRds);
        prj = prj.Replace("##Process##", prcs);

        //Template
        prj = prj.Replace("##TemplateFile##", cmn.GetTags("TemplateFile").Replace("##TemplateFileName##", tmpltFlNm));
        if (!string.IsNullOrEmpty(prjcts)) { prjcts = string.Format("{0}\r\n{1}", prjcts, prj); }
        else { prjcts = prj; }
      }
    }

    private void CreateDataTableStructures()//1
    {
      List<DataTableField> flds = new List<DataTableField>();
      DataTableField dtFld;
      Common cmn = new Common();

      //Status Data Table
      dtFld = new DataTableField();
      dtFld.Name = "Project Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "First POS File"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Last POS File"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      missionsDt = cmn.CreateNewDataTable(flds, missionsDtNm);
    }

    public string GetProjects() { return prjcts; }

    private void InitializeMissionsDataTable()//2
    {
      List<FileInfo> mFls = new DirectoryInfo(btchPCSFldrNm).GetFiles().OrderBy(f => f.FullName).ToList();
      posFlNms = new List<FileInfo>();
      lstFlNm = string.Empty;
      if (mFls.Count > 0)
      {
        foreach (FileInfo mFl in mFls)
        {
          if (string.IsNullOrEmpty(lstFlNm)) { lstFlNm = Path.GetFileNameWithoutExtension(mFl.FullName).ToUpper(); }
          if (lstFlNm != Path.GetFileNameWithoutExtension(mFl.FullName).ToUpper())
          {
            UpdateMissionsDataTable();
            lstFlNm = Path.GetFileNameWithoutExtension(mFl.FullName).ToUpper();
            posFlNms = new List<FileInfo>();
          }
          posFlNms.Add(mFl);
        }
        UpdateMissionsDataTable();
      }
    }

    private void MovePOSFilesToSkippedFolder()
    {
      string skipFldrNm = Path.Combine(btchPCSFldrNm,"Skipped");
      if (!Directory.Exists(skipFldrNm)) { Directory.CreateDirectory(skipFldrNm); }
      foreach (FileInfo posFlNm in posFlNms)
      {
        string toFlNm = Path.Combine(skipFldrNm, posFlNm.Name);
        File.Move(posFlNm.FullName, toFlNm);
      }
    }

    private void SetupTheBatchMissions()//first
    {
      CreateDataTableStructures();
      InitializeMissionsDataTable();
      AddBatchProjectsFromMissions();
    }

    private void UpdateMissionsDataTable()
    {
      if (posFlNms.Count > 2)
      {
        DataRow dr = missionsDt.NewRow();
        dr["Project Name"] = lstFlNm;
        dr["First POS File"] = posFlNms[0].FullName;
        dr["Last POS File"] = posFlNms[posFlNms.Count - 1].FullName;
        missionsDt.Rows.Add(dr);
      }
      else
      {
        MovePOSFilesToSkippedFolder();
      }
    }
    #endregion

  }
}
