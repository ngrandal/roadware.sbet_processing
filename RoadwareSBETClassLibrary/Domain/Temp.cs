﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RoadwareSBETClassLibrary.Domain
{
  public class Temp
  {
    private DataSet btchDs;
    private DataSet btchBadDs;

    private DataTable batDt;
    private DataTable batBadDt;
    private DataTable prjDt;
    private DataTable prjBadDt;
    private DataTable extrctDt;
    private DataTable extrctBadDt;
    private DataTable prcsDt;
    private DataTable prcsBadDt;
    private DataTable smrtBsDt;
    private DataTable smrtBsBadDt;
    private DataTable stgDt;
    private DataTable stgBadDt;

    private string btchPrjFullFlNm;
    private string[,] dtNms = new string[6, 3] 
    { 
      {"Batch","batDt","batBadDt"},
      {"Project","prjDt","prjBadDt"},
      {"Extract","extrctDt","extrctBadDt"},
      {"Process","prcsDt","prcsBadDt"},
      {"Smartbase","smrtBsDt","smrtBsBadDt"},
      {"Stage","stgDt","stgBadDt"}
    };

    public Temp(string batchProjectFullFileName)
    {
      btchPrjFullFlNm = batchProjectFullFileName;
      LoadBatchMissionData();
    }


    private void LoadBatchMissionData()
    {
      btchDs = new DataSet();
      btchDs.ReadXml(btchPrjFullFlNm);
      btchBadDs = btchDs.Clone();

      for (int i = 0; i < dtNms.GetLength(0); i++)
      {
        //Debug.WriteLine(string.Format("{0},{1},{2}", dtNms[i,0], dtNms[i,1], dtNms[i,2]));
        if (btchDs.Tables.Contains(dtNms[i, 0]))
        {
          FieldInfo fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 1], BindingFlags.NonPublic | BindingFlags.Instance);
          fiDt.SetValue(this, btchDs.Tables[dtNms[i, 0]]);

          fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 2], BindingFlags.NonPublic | BindingFlags.Instance);
          fiDt.SetValue(this, btchBadDs.Tables[dtNms[i, 0]]);
        }
      }
    }

  }
}
