﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoadwareSBETClassLibrary.Domain;

namespace RoadwareSBETClassLibrary.Domain
{
  public class MigratePOSFiles
  {
    #region fields
    private DataSet migrateDs;

    private DataTable statusDt;

    private int frmDt;
    private int toDt;

    private List<string> batchFldrNms;
    private List<string> dtaFldrNms;

    private string aranFldrNm;
    private string aranStatusXMLFlNm = "SBET_Status.xml";
    private string batchFldrNm;
    private string batchNm;
    private string cmnt= string.Empty;
    private string pcsFldrNm;
    private string posDataFldrNm;
    private string statusDtNm = "ARAN SBET Status";
    #endregion

    #region constructors
    public MigratePOSFiles(string localAranIdFolderNm, string localBatcFolderNm, string serverVideoLocationFolderNm,
                           string dataCollectionFromDate, string dataCollectionToDate, string pcsFolderNm)
    {
      aranFldrNm = localAranIdFolderNm;
      batchFldrNm = localBatcFolderNm;
      //pcsFldrNm = Path.Combine(batchFldrNm, "PCS");
      pcsFldrNm = pcsFolderNm;
      posDataFldrNm = Path.Combine(serverVideoLocationFolderNm, "Posdata");
      frmDt = GetNumberFromDateString(dataCollectionFromDate);
      toDt = GetNumberFromDateString(dataCollectionToDate);
      string[] flPrts = batchFldrNm.Split('\\');
      batchNm = flPrts[flPrts.Length - 1];
      MigrateThePOSFiles();
    }

      //new constructor for 2014 new posdata structure
    public MigratePOSFiles(string localAranIdFolderNm, string localBatcFolderNm, string serverVideoLocationFolderNm,
                         string dataCollectionFromDate, string dataCollectionToDate, string ARAN, string pcsFolderNm)
    {
        aranFldrNm = localAranIdFolderNm;
        batchFldrNm = localBatcFolderNm;
        //pcsFldrNm = Path.Combine(batchFldrNm, "PCS");
        pcsFldrNm = pcsFolderNm;
        posDataFldrNm = Path.Combine(serverVideoLocationFolderNm, ARAN.Replace(" ", "") + "_Posdata");
        frmDt = GetNumberFromDateString(dataCollectionFromDate);
        toDt = GetNumberFromDateString(dataCollectionToDate);
        string[] flPrts = batchFldrNm.Split('\\');
        batchNm = flPrts[flPrts.Length - 1];
        MigrateThePOSFiles();
    }
    #endregion

    #region methods
    private void AddComment(string newCmnt)
    {
      if (!string.IsNullOrEmpty(cmnt)) { cmnt = string.Format("{0}; {1}", cmnt, newCmnt); }
      else { cmnt = newCmnt; }
    }

    private void AddFolderToStatusDataTable(string fldr, int fldrDtNbr)//1.2.1.4
    {
      DataRow dr = statusDt.NewRow();
      dr["Date Number"] = fldrDtNbr;
      dr["Date Folder Name"] = fldr;
      dr["Processed"] = false;
      dr["Batch Name(s)"] = string.Empty;
      statusDt.Rows.Add(dr);
    }

    private void CheckBatchDates()//1.3.1
    {
      //using (StreamWriter sw = File.AppendText(Path.Combine(batchFldrNm, "FileList.log")))
      string[] fldrPrts = pcsFldrNm.Split('\\');
      Array.Resize(ref fldrPrts, fldrPrts.Length - 1);

      using (StreamWriter sw = File.AppendText(Path.Combine(string.Join("\\",fldrPrts), "FileList.log")))
      {
        sw.Write(string.Format("From Date: {0}, To Date: {1}\r\n", frmDt, toDt));
        foreach (DataRow dr in statusDt.Rows)
        {
          sw.Write(string.Format("Status Date: {0}\r\n", dr["Date Number"].ToString()));
        }
      }
      string fltr = string.Format("[Date Number] = {0}", frmDt);
      DataRow[] drs = statusDt.Select(fltr);
      if (drs.Length == 0) { AddComment("Data Collection From Date not found"); }
      fltr = string.Format("[Date Number] = {0}", toDt);
      drs = statusDt.Select(fltr);
      if (drs.Length == 0) { AddComment("Data Collection To Date not found"); }
    }

    private void CheckStatusForNewFolders()//1.2.2.2
    {
      //using (StreamWriter sw = File.AppendText(Path.Combine(batchFldrNm, "FileList.log")))
      //{
      //  sw.WriteLine("Checking status for new folders.");
      //}
      //using (StreamWriter sw = File.AppendText(Path.Combine(batchFldrNm, "FileList.log")))
      //{
      //  sw.WriteLine(string.Format("{0} Posdata folders to check.", dtaFldrNms.Count));

        foreach (string fldr in dtaFldrNms)
        {
          //sw.WriteLine(string.Format("Checking {0} as folder date number.",fldr));
          int fldrDtNbr = GetFolderDateNumber(fldr);
          if (fldrDtNbr > 0)
          {
            string fltr = string.Format("[Date Number] = '{0}'", fldrDtNbr);
            DataRow[] drs = statusDt.Select(fltr);
            if (drs.Length == 0)
            {
              AddFolderToStatusDataTable(fldr, fldrDtNbr);
            }
          }
        }
      //}
    }

    //private void CopyPOSDataToLocalWorkstation()//1.4
    private void CopyPOSDataToProjectServer()
    {
      //using (StreamWriter sw = File.AppendText(Path.Combine(batchFldrNm, "FileList.log")))
      string[] fldrPrts = pcsFldrNm.Split('\\');
      Array.Resize(ref fldrPrts, fldrPrts.Length - 1);

      using (StreamWriter sw = File.AppendText(Path.Combine(string.Join("\\",fldrPrts), "FileList.log")))
      {
        foreach (string fldr in batchFldrNms)
        {
          DirectoryInfo dirInfo = new DirectoryInfo(fldr);
          FileInfo[] frmFls = dirInfo.GetFiles();
          sw.Write(string.Format("Getting {0} files from {1} folder.\r\n", frmFls.Length, fldr));
          foreach (FileInfo frmFl in frmFls)
          {
            string toFlNm = Path.Combine(pcsFldrNm, frmFl.Name);

            sw.Write(string.Format("Copying {0} to {1}\r\n", frmFl.FullName, toFlNm));
            File.Copy(frmFl.FullName, toFlNm, true);
          }
        }
      }
    }

    private void CreateDataTableStructures()//1.2.1.2
    {
      migrateDs = new DataSet();
      List<DataTableField> flds = new List<DataTableField>();
      DataTableField dtFld;
      Common cmn = new Common();

      //Status Data Table
      dtFld = new DataTableField();
      dtFld.Name = "Date Number"; dtFld.Type = typeof(System.Int32); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Date Folder Name"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Processed"; dtFld.Type = typeof(System.Boolean); flds.Add(dtFld);
      dtFld = new DataTableField();
      dtFld.Name = "Batch Name(s)"; dtFld.Type = typeof(System.String); flds.Add(dtFld);
      statusDt = cmn.CreateNewDataTable(flds, statusDtNm);
      migrateDs.Tables.Add(statusDt);
    }

    public string GetComments() { return cmnt; }

    private int GetFolderDateNumber(string fldr)//1.2.1.3
    {
      string[] fldrPrts = fldr.Split('\\');
      int fldrDtNbr=0;
      int.TryParse(fldrPrts[fldrPrts.Length - 1], out fldrDtNbr);
      return fldrDtNbr;
    }

    private int GetNumberFromDateString(string dt)
    {
      //  dd/mm/yyyy - English (U.K.)
      //  mm/dd/yyyy - English (U.S.)
      string[] dtPrts = dt.Split('/');
      return int.Parse(string.Format("{0}{1}{2}",
                                     dtPrts[2].Substring(2,2),
                                     dtPrts[1].PadLeft(2,'0'),
                                     dtPrts[0].PadLeft(2,'0')));
    }

    private void InitializeStatusDataTable(bool addFldrNms = true)//1.2.1.1
    {
      CreateDataTableStructures();
      if (addFldrNms)
      {
        foreach (string fldr in dtaFldrNms)
        {
          int fldrDtNbr = GetFolderDateNumber(fldr);
          if (fldrDtNbr > 0)
          {
            AddFolderToStatusDataTable(fldr, fldrDtNbr);
          }
        }
      }
    }

    private void LoadPosDataDateFolderList()//1.1
    {
      dtaFldrNms = new List<string>();
      if (Directory.Exists(posDataFldrNm))
      {
        foreach (string fldr in Directory.GetDirectories(posDataFldrNm).OrderBy(f => f).ToArray())
        {
          dtaFldrNms.Add(fldr);
        }
      }
      else
      {
        MessageBox.Show(string.Format("\"{0}\" folder is missing.  Check SalesForce or server to resolve.",posDataFldrNm ), "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void LoadBatchDateFolderList()//1.3
    {
      CheckBatchDates();
      string fltr = string.Format("[Date Number] >= {0} AND [Date Number] <= {1}", frmDt, toDt);
      DataRow[] drs = statusDt.Select(fltr);
      batchFldrNms = new List<string>();
      foreach (DataRow dr in drs)
      {
        string dt = dr["Date Number"].ToString();
        dt = string.Format("{0}/{1}/20{2}",
                           dt.Substring(2,2),
                           dt.Substring(4,2),
                           dt.Substring(0,2));
        if (bool.Parse(dr["Processed"].ToString()))
        {
          AddComment(string.Format("{0} processed by another batch", dt));
          dr["Batch Name(s)"] = string.Format("{0}; {1}", dr["Batch Name(s)"].ToString(), batchNm);
        }
        else
        {
          batchFldrNms.Add(dr["Date Folder Name"].ToString());
          dr["Processed"] = true;
          dr["Batch Name(s)"] = batchNm;
        }
      }
    }

    private void LoadStatusDataTable(string aranStatusXMLFullFlNm)//1.2.2.1
    {
      migrateDs = new DataSet();
      migrateDs.ReadXml(aranStatusXMLFullFlNm);
      if (migrateDs.Tables.Contains(statusDtNm))
      {
        statusDt = migrateDs.Tables[statusDtNm];
      }
      else
      {
        InitializeStatusDataTable(false);
      }

    }

    private void MigrateThePOSFiles()//1
    {
      LoadPosDataDateFolderList();
      VerifyStatusDataTable();
      LoadBatchDateFolderList();
      //CopyPOSDataToLocalWorkstation();
      CopyPOSDataToProjectServer();
      SaveDataTableStructures();
    }

    private void SaveDataTableStructures()//1.5
    {
      string aranStatusXMLFullFlNm = Path.Combine(aranFldrNm, aranStatusXMLFlNm);
      migrateDs.WriteXml(aranStatusXMLFullFlNm);
    }

    private void VerifyStatusDataTable()//1.2
    {
      string aranStatusXMLFullFlNm = Path.Combine(aranFldrNm, aranStatusXMLFlNm);
      if (File.Exists(aranStatusXMLFullFlNm))
      {
        LoadStatusDataTable(aranStatusXMLFullFlNm);
        CheckStatusForNewFolders();
      }
      else
      {
        InitializeStatusDataTable();
      }
    }
    #endregion
  }
}
