﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoadwareSBETClassLibrary.Domain
{
  public class ExportBatchMissions
  {
    #region fields
    private bool snglMsn = false;

    private string btchNm = string.Empty;
    private string posPacExeFlNm;
    private string prjBatFldrNm;
    private string pythonExeFlNm;
    private string pyExpScrFlNm;
    private string snglMsnNm = string.Empty;
    private string vdoSrvrFldrNm = string.Empty;
    #endregion

    #region constructors
    public ExportBatchMissions(string pythonExecutableFileName, string pythonExportScriptFileName, 
                               string projectBatchFolderName, string posPacExecutableFileName)
    {
      pythonExeFlNm = pythonExecutableFileName;
      pyExpScrFlNm = pythonExportScriptFileName;
      prjBatFldrNm = projectBatchFolderName;
      posPacExeFlNm = posPacExecutableFileName;
      ExportTheBatchMissions();
    }
    public ExportBatchMissions(string pythonExecutableFileName, string pythonExportScriptFileName,
                               string projectBatchFolderName, string posPacExecutableFileName, bool singleMission)
    {
      pythonExeFlNm = pythonExecutableFileName;
      pyExpScrFlNm = pythonExportScriptFileName;
      prjBatFldrNm = projectBatchFolderName;
      posPacExeFlNm = posPacExecutableFileName;
      snglMsn = singleMission;
      ExportTheBatchMissions();
    }
    public ExportBatchMissions(string pythonExecutableFileName, string pythonExportScriptFileName, 
                               string projectBatchFolderName, string posPacExecutableFileName,
                               string videoServerFolderName)
    {
      pythonExeFlNm = pythonExecutableFileName;
      pyExpScrFlNm = pythonExportScriptFileName;
      prjBatFldrNm = projectBatchFolderName;
      posPacExeFlNm = posPacExecutableFileName;
      vdoSrvrFldrNm = videoServerFolderName;
      ExportTheBatchMissions();
    }
    public ExportBatchMissions(string pythonExecutableFileName, string pythonExportScriptFileName,
                               string projectBatchFolderName, string posPacExecutableFileName,
                               string videoServerFolderName, bool singleMission)
    {
      pythonExeFlNm = pythonExecutableFileName;
      pyExpScrFlNm = pythonExportScriptFileName;
      prjBatFldrNm = projectBatchFolderName;
      posPacExeFlNm = posPacExecutableFileName;
      vdoSrvrFldrNm = videoServerFolderName;
      snglMsn = singleMission;
      ExportTheBatchMissions();
    }
    public ExportBatchMissions(string pythonExecutableFileName, string pythonExportScriptFileName,
                               string projectBatchFolderName, string posPacExecutableFileName,
                               string videoServerFolderName, string batchName)
    {
      pythonExeFlNm = pythonExecutableFileName;
      pyExpScrFlNm = pythonExportScriptFileName;
      prjBatFldrNm = projectBatchFolderName;
      posPacExeFlNm = posPacExecutableFileName;
      vdoSrvrFldrNm = videoServerFolderName;
      btchNm = batchName;
      ExportTheBatchMissions();
    }
    #endregion

    #region methods
    private void ExportTheBatchMissions()
    {
      POSPacExport();
      if (!string.IsNullOrEmpty(vdoSrvrFldrNm)) { MigrateExportDataToVideoServer(); }
    }

    private void MigrateExportDataToVideoServer()
    {
      string toExpFldrNm = string.Empty;
      if (!string.IsNullOrEmpty(btchNm))
      {
        if (Directory.Exists(vdoSrvrFldrNm))
        {
          toExpFldrNm = Path.Combine(vdoSrvrFldrNm, "Data");
          if (!Directory.Exists(toExpFldrNm)) { Directory.CreateDirectory(toExpFldrNm); }

          toExpFldrNm = string.Format(@"{0}\Batch_{1}", toExpFldrNm, btchNm);
          if (!Directory.Exists(toExpFldrNm)) { Directory.CreateDirectory(toExpFldrNm); }

          toExpFldrNm = Path.Combine(toExpFldrNm, "Export");
          if (!Directory.Exists(toExpFldrNm)) { Directory.CreateDirectory(toExpFldrNm); }
        }
      }
      else
      {
        toExpFldrNm = vdoSrvrFldrNm;
      }

      if (!string.IsNullOrEmpty(btchNm) || string.IsNullOrEmpty(snglMsnNm))
      {
        foreach (string fldrNm in Directory.GetDirectories(prjBatFldrNm))
        {
          string[] fldrPrts = fldrNm.Split('\\');
          string frmExpFldrNm = string.Format(@"{0}\{1}\Export",
                                              fldrNm,
                                              fldrPrts[fldrPrts.Length - 1]);
          if (Directory.Exists(frmExpFldrNm))
          {
            foreach (FileInfo frmExpFlNm in new DirectoryInfo(frmExpFldrNm).GetFiles())
            {
              string toExpFlNm = Path.Combine(toExpFldrNm, frmExpFlNm.Name);
              File.Copy(frmExpFlNm.FullName, toExpFlNm, true);
            }
          }
        }
      }
      if (!string.IsNullOrEmpty(snglMsnNm))
      {
        string frmExpFldrNm = Path.Combine(Path.Combine(Path.Combine(prjBatFldrNm, snglMsnNm), snglMsnNm), "Export");

        //MessageBox.Show(string.Format("From export folder name: {0}",frmExpFldrNm));

        if (Directory.Exists(frmExpFldrNm))
        {
          foreach (FileInfo frmExpFlNm in new DirectoryInfo(frmExpFldrNm).GetFiles())
          {
            string toExpFlNm = Path.Combine(toExpFldrNm, frmExpFlNm.Name);
            File.Copy(frmExpFlNm.FullName, toExpFlNm, true);
          }
        }
      }
    }

    private void POSPacExport()
    {
      if (snglMsn)
      {
        string[] fldrPrts = prjBatFldrNm.Split('\\');
        snglMsnNm = fldrPrts[fldrPrts.Length - 1];
        Array.Resize(ref fldrPrts, fldrPrts.Length - 1);
        prjBatFldrNm = string.Join("\\", fldrPrts);
      }
      Process p = new Process();
      p.StartInfo.FileName = pythonExeFlNm;
      if (string.IsNullOrEmpty(snglMsnNm))
      {
        //MessageBox.Show(string.Format("python exe: {0}", pythonExeFlNm));
        //MessageBox.Show(string.Format("python script: {0}", pyExpScrFlNm));
        //MessageBox.Show(string.Format("batch project folder: {0}", prjBatFldrNm));
        //MessageBox.Show(string.Format("posPac exe: {0}", posPacExeFlNm));

        p.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
                                                                         pyExpScrFlNm,
                                                                         prjBatFldrNm,
                                                                         posPacExeFlNm);
      }
      else
      {
        p.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"",
                                                                                pyExpScrFlNm,
                                                                                prjBatFldrNm,
                                                                                posPacExeFlNm,
                                                                                snglMsnNm);
      }

      p.Start();
      p.WaitForExit();
      p.Close();
      p.Dispose();
    }
    #endregion
  }
}
